window.onload = () => {
  print();
};

// add a new one
document.querySelector('form').onsubmit = (event) => {
  event.preventDefault();
  const values = Object.values(event.target).reduce(
    (acc, input) =>
      !input.name
        ? acc
        : {
            ...acc,
            [input.name]: input.value,
          },
    {}
  );
  axios.post('http://localhost:3800/task', values).then((res) => {
    axios.get('http://localhost:3800/task').then((res) => {
      print();
    });
  });
  document.querySelector('form').reset();
};

// print to the html
function print() {
  let toDoListArryServer;
  axios.get('http://localhost:3800/task').then((res) => {
    toDoListArryServer = res.data;
    // not done print to the todo
    document.getElementById('toDo').innerHTML = toDoListArryServer
      .map((item) => {
        if (item.doing == false) {
          return ` <label class="b-contain" for="${item.id}"> <span class="btn-x" onclick="deleteTaskById(${item.id})" ><i class="far fa-times-circle"></i></span>  <span class="name" onclick="changeDoing(${item.id})">${item.name}</span>  <div class="b-input"></div>  </label> `;
        }
      })
      .join('');
    // if its already done print to do already
    document.getElementById('doAlready').innerHTML = toDoListArryServer
      .map((item) => {
        if (item.doing == true) {
          return `<label class="b-contain" for="${item.id}"> <span class="btn-x" onclick="deleteTaskById(${item.id})" ><i class="far fa-times-circle"></i></span>  <span class="name" onclick="changeDoing(${item.id})">${item.name}</span>  <div class="b-input"></div>  </label>`;
        }
      })
      .join('');
  });
}

// on check change to doing = false/true and print again
function changeDoing(_id) {
  axios.put('http://localhost:3800/task', {id: _id}).then((res) => {
    print();
  });
}

// delete one item onclick by id
function deleteTaskById(_id) {
  axios.delete('http://localhost:3800/task', {id: _id}).then((res) => {
    print();
  });
}

// make all of them as didnt do it
function clearTheList() {
  axios.put('http://localhost:3800/task/all').then((res) => {
    print();
  });
}

// delete all of the list by click
function deleteTheList() {
  axios.delete('http://localhost:3800/task/all').then((res) => {
    print();
  });
}
