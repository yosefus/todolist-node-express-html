const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');

app.use(cors());
app.use(express.json());
list = [];
let counter = 200;

list.push(new AddNew('buy some teeth'));
counter++;
list.push(new AddNew('dance on the roof'));
counter++;
list.push(new AddNew('call to micki mouse'));
counter++;

function AddNew(newName) {
  (this.name = newName), (this.id = counter), (this.doing = false);
}

// get the list
app.get('/task', function (req, res) {
  res.send(list);
});

// add a new one
app.post('/task', function (req, res) {
  counter++;
  let newTask = new AddNew(req.body.name);
  list.push(newTask);
  res.send(list);
});

// delete by id
app.delete('/task', function (req, res) {
  list.splice(
    list.findIndex((element) => element.id == req.body.id),
    1
  );
  res.send(list);
});

// delete all
app.delete('/task/all', function (req, res) {
  list = [];
  res.send(list);
});

// change the doing status by id
app.put('/task', function (req, res) {
  let found = list.find((element) => element.id == req.body.id);
  found.doing = !found.doing;
  res.send(list);
});

// change the doing status
app.put('/task/all', function (req, res) {
  list.forEach((element) => {
    element.doing = false;
  });
  res.send(list);
});

app.listen(3800, () => {
  console.log('server runing');
});
